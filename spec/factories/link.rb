FactoryGirl.define do
  factory :link do
    name         { FFaker::InternetSE.company_name_single_word }
    original_url { FFaker::InternetSE.http_url }
    group

    trait :updated do
      name 'Updated'
    end

    trait :invalid do
      name         ''
      original_url ''
    end
  end
end
