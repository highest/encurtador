FactoryGirl.define do
  factory :group do
    name { FFaker::Color.name }

    trait :updated do
      name 'Updated'
    end

    trait :invalid do
      name ''
    end
  end
end
