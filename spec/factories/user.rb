FactoryGirl.define do
  factory :user do
    sequence(:name)       { |n| "#{FFaker::Name.name} #{n}" }
    email                 { FFaker::Internet.safe_email(name) }
    password              { FFaker::Internet.password }
    password_confirmation { password }
    current_password      { password }
    salt                  'asdasdastr4325234324sdfds'
    crypted_password      { Sorcery::CryptoProviders::BCrypt.encrypt(password, salt) }

    after(:create) do |user|
      user.generate_reset_password_token!
    end

    trait :invalid do
      name ''
    end
  end
end
