FactoryGirl.define do
  factory :visit do
    ip         '178.253.156.190'
    user_agent 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)'
    referer    ''
    browser    'Mozilla'
    platform   'Windows'
    location   'Poprad/Slovak Republic'
    source     'Acesso Direto'
    link

    trait :ankara do
      ip       '85.105.122.72'
      location 'Ankara/Turkey'
    end

    trait :dearborn do
      ip       '19.25.244.21'
      location 'Dearborn/United States'
    end

    trait :gloucester do
      ip       '159.219.255.149'
      location 'Gloucester/United Kingdom'
    end

    trait :greenwood do
      ip       '98.220.43.221'
      location 'Greenwood/United States'
    end

    trait :unidentified do
      ip       '127.0.0.1'
      location 'Não Identificado'
    end
  end
end
