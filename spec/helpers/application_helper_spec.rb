require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#stats_table' do
    before { create_list(:visit, 3) }

    it 'displays the browser`s stats' do
      expect(helper.stats_table(Visit.all.group(:browser).count)).to eq([['Mozilla', 3, 100.0]])
    end

    it 'displays the platform`s stats' do
      expect(helper.stats_table(Visit.all.group(:platform).count)).to eq([['Windows', 3, 100.0]])
    end

    it 'displays the source`s stats' do
      expected = [['Acesso Direto', 3, 100.0]]
      expect(helper.stats_table(Visit.all.group(:source).count)).to eq(expected)
    end

    it 'displays the location`s stats' do
      create(:visit, :ankara)
      create(:visit, :dearborn)
      create(:visit, :gloucester)
      create(:visit, :greenwood)
      create(:visit, :unidentified)

      expected = [
        ['Poprad/Slovak Republic', 3, 37.5],
        ['Gloucester/United Kingdom', 1, 12.5],
        ['Greenwood/United States', 1, 12.5],
        ['Não Identificado', 1, 12.5],
        ['Outros', 2, 25.0]
      ]

      expect(helper.stats_table(Visit.all.group(:location).count)).to eq(expected)
    end
  end
end
