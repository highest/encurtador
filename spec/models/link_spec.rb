require 'rails_helper'

RSpec.describe Link, type: :model do
  context 'associations' do
    subject { build(:link) }

    it { should belong_to(:group).counter_cache(true) }
    it { should have_many(:visits).dependent(:destroy) }
  end

  context 'validations' do
    subject { build(:link) }

    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:original_url) }
    it { should validate_presence_of(:slug) }
    it { should validate_uniqueness_of(:slug) }
    it { should accept_nested_attributes_for(:group) }
  end

  describe '#should_create_group?' do
    let(:link) { create(:link) }
    let(:group) { create(:group) }

    context 'when group_id is present' do
      it do
        link.group_id = group.id
        expect(link.should_create_group?).to eq(true)
      end
    end

    context 'when group_id is not present' do
      it do
        link.group_id = nil
        expect(link.should_create_group?).to eq(false)
      end
    end
  end
end
