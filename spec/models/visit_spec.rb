require 'rails_helper'

RSpec.describe Visit, type: :model do
  let(:visit) { create(:visit) }

  context 'validations' do
    subject { build(:visit) }

    it { should belong_to(:link).counter_cache(true) }
  end

  describe '#generate_browser_and_platform' do
    it 'sets the browser and platform fields' do
      visit.user_agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)'
      visit.generate_browser_and_platform

      expect(visit.browser).to eq('Internet Explorer')
      expect(visit.platform).to eq('Windows')
    end
  end

  describe '#generate_location' do
    context 'when a valid IP is present' do
      it do
        visit.ip = '178.253.156.190'
        visit.generate_location

        expect(visit.location).to eq('Poprad/Slovak Republic')
      end
    end

    context 'when an invalid IP is present' do
      it do
        visit.ip = '127.0.0.1'
        visit.generate_location

        expect(visit.location).to eq('Não Identificado')
      end
    end
  end

  describe '#generate_source' do
    context 'when referer is present' do
      it do
        visit.referer = 'http://www.google.com.br'
        visit.generate_source

        expect(visit.source).to eq('google.com.br')
      end
    end

    context 'when referer is not present' do
      it do
        visit.referer = ''
        visit.generate_source

        expect(visit.source).to eq('Acesso Direto')
      end
    end
  end

  describe '#get_host_without_www' do
    context 'when url is present' do
      it do
        url = 'http://www.google.com.br'
        expect(visit.get_host_without_www(url)).to eq('google.com.br')
      end
    end

    context 'when url is not present' do
      it do
        url = ''
        expect(visit.get_host_without_www(url)).to eq('Acesso Direto')
      end
    end
  end

  describe '#generate_metadata' do
    it 'sets the metadatas' do
      visit.ip = '178.253.156.190'
      visit.referer = ''
      visit.user_agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)'

      visit.generate_metadata

      expect(visit.browser).to eq('Internet Explorer')
      expect(visit.location).to eq('Poprad/Slovak Republic')
      expect(visit.platform).to eq('Windows')
      expect(visit.source).to eq('Acesso Direto')
    end
  end

  describe '.recent' do
    before { create_list(:visit, 3) }

    context 'when a limit is specified' do
      it { expect(Visit.recent(2).size).to eq(2) }
    end

    context 'when a limit is not specified' do
      it { expect(Visit.recent.size).to eq(3) }
    end
  end
end
