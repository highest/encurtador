require 'rails_helper'

RSpec.describe Group, type: :model do
  context 'associations' do
    subject { build(:group) }

    it { should have_many(:links).dependent(:destroy) }
  end

  context 'validations' do
    subject { build(:group) }

    it { should validate_presence_of(:name) }
  end
end
