require 'rails_helper'

RSpec.describe PasswordResetsController, type: :controller do
  let(:user) { create(:user) }

  describe 'GET #new' do
    before do
      login_user(user)
      get :new
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'POST #create' do
    context 'with valid data' do
      before do
        login_user(user)
        post :create, params: { email: user.email }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(login_url) }
      it { expect(flash[:notice]).to eq('Instruções foram enviadas para o seu e-mail') }
    end

    context 'with invalid data' do
      before do
        login_user(user)
        post :create, params: { email: '' }
      end

      it { expect(response).to have_http_status(422) }
      it { expect(flash[:alert]).to eq('Reset password failed') }
    end
  end

  describe 'GET #edit' do
    context 'with valid data' do
      before do
        login_user(user)
        get :edit, params: { id: user.reset_password_token }
      end

      it { expect(response).to have_http_status(200) }
    end

    context 'with invalid data' do
      before do
        login_user(user)

        user.reset_password_token = 'wrong'
        get :edit, params: { id: user.reset_password_token }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(login_url) }
      it { expect(flash[:alert]).to eq('You have to authenticate to access this page.') }
    end
  end

  describe 'PATCH #update' do
    context 'with valid data' do
      before do
        login_user(user)
        patch :update, params: {
          id: user.reset_password_token,
          user: { password: 'updated', password_confirmation: 'updated' }
        }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(login_url) }
      it { expect(flash[:notice]).to eq('Senha foi atualizada com sucesso.') }
    end

    context 'with invalid data' do
      before do
        login_user(user)
        patch :update, params: {
          id: user.reset_password_token,
          user: { password: 'updated', password_confirmation: '' }
        }
      end

      it { expect(response).to have_http_status(422) }
      it { expect(flash[:alert]).to eq('Edit password failed') }
    end
  end
end
