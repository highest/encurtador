require 'rails_helper'

RSpec.describe Users::SessionsController, type: :controller do
  describe 'GET #new' do
    before(:each) do
      create(:user)
      get :new
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'POST #create' do
    context 'with valid data' do
      before do
        user = create(:user)
        post :create, params: { email: user.email, password: user.current_password }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(links_url) }
      it { expect(flash[:notice]).to eq('Login successful') }
    end

    context 'with invalid data' do
      before do
        create(:user)
        post :create, params: { email: '', password: '' }
      end

      it { expect(response).to have_http_status(422) }
      it { expect(flash[:alert]).to eq('Login failed') }
    end
  end

  describe 'DELETE #destroy' do
    before do
      login_user(create(:user))
      delete :destroy
    end

    it { expect(response).to have_http_status(302) }
    it { expect(response).to redirect_to(login_url) }
    it { expect(flash[:notice]).to eq('Logged out!') }
  end
end
