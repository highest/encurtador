require 'rails_helper'

RSpec.describe Users::PasswordsController, type: :controller do
  let(:user) { create(:user) }

  describe 'GET #edit' do
    before do
      login_user(user)
      get :edit
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'PATCH #update' do
    context 'with valid data' do
      before do
        login_user(user)
        patch :update, params: {
          user: {
            current_password: user.current_password,
            password: 'updated',
            password_confirmation: 'updated'
          }
        }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(edit_users_password_url) }
      it { expect(flash[:notice]).to eq('Senha atualizada com sucesso') }
    end

    context 'with invalid data' do
      before do
        login_user(user)
        patch :update, params: {
          user: {
            current_password: user.current_password,
            password: '',
            password_confirmation: ''
          }
        }
      end

      it { expect(response).to have_http_status(422) }
    end
  end
end
