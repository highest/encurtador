require 'rails_helper'

RSpec.describe Users::RegistrationsController, type: :controller do
  let(:user) { create(:user) }

  describe 'GET #edit' do
    before do
      login_user(user)
      get :edit
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'PATCH #update' do
    context 'with valid data' do
      before do
        login_user(user)
        patch :update, params: {
          user: {
            name: 'John Doe',
            email: 'john.doe@email.com',
            current_password: user.current_password
          }
        }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(edit_users_registration_url) }
      it { expect(flash[:notice]).to eq('Perfil atualizado com sucesso') }
    end

    context 'with invalid data' do
      before do
        login_user(user)
        patch :update, params: {
          user: {
            name: 'John Doe',
            email: 'john.doe@email.com',
            current_password: ''
          }
        }
      end

      it { expect(response).to have_http_status(422) }
    end
  end
end
