require 'rails_helper'

RSpec.describe DashboardController, type: :controller do
  let(:user) { create(:user) }

  describe 'GET #show' do
    before do
      login_user(user)
      create(:visit)
      get :show
    end

    it { expect(response).to have_http_status(200) }
  end
end
