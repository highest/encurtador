require 'rails_helper'

RSpec.describe GroupsController, type: :controller do
  let(:group) { create(:group) }
  let(:user) { create(:user) }

  describe 'GET #index' do
    before do
      login_user(user)
      get :index
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'GET #show' do
    before do
      login_user(user)
      get :show, params: { id: group.id }
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'GET #new' do
    before do
      login_user(user)
      get :new
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'GET #edit' do
    before do
      login_user(user)
      get :edit, params: { id: group.id }
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'POST #create' do
    context 'with valid data' do
      before do
        login_user(user)
        post :create, params: { group: attributes_for(:group) }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(group_url(Group.last)) }
      it { expect(flash[:notice]).to eq('Group was successfully created.') }
    end

    context 'with invalid data' do
      before do
        login_user(user)
        post :create, params: { group: attributes_for(:group, :invalid) }
      end

      it { expect(response).to have_http_status(422) }
    end
  end

  describe 'PATCH #update' do
    context 'with valid data' do
      before do
        login_user(user)
        patch :update, params: { id: group.id, group: attributes_for(:group, :updated) }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(group_url(group)) }
      it { expect(flash[:notice]).to eq('Group was successfully updated.') }
    end

    context 'with invalid data' do
      before do
        login_user(user)
        patch :update, params: { id: group.id, group: attributes_for(:group, :invalid) }
      end

      it { expect(response).to have_http_status(422) }
    end
  end

  describe 'DELETE #destroy' do
    before do
      login_user(user)
      delete :destroy, params: { id: group.id }
    end

    it { expect(response).to have_http_status(302) }
    it { expect(response).to redirect_to(groups_url) }
    it { expect(flash[:notice]).to eq('Group was successfully destroyed.') }
  end
end
