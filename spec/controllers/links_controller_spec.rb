require 'rails_helper'

RSpec.describe LinksController, type: :controller do
  let(:link) { create(:link) }
  let(:user) { create(:user) }

  describe 'GET #index' do
    before do
      login_user(user)
      get :index
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'GET #show' do
    before do
      login_user(user)
      get :show, params: { id: link.id }
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'GET #visit' do
    before do
      login_user(user)
      get :visit, params: { slug: link.slug }
    end

    it { expect(response).to have_http_status(301) }
    it { expect(response).to redirect_to(link.original_url) }
  end

  describe 'GET #new' do
    before do
      login_user(user)
      get :new, params: { link: attributes_for(:link) }
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'GET #edit' do
    before do
      login_user(user)
      get :edit, params: { id: link.id }
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'POST #create' do
    context 'with valid data' do
      before do
        login_user(user)
        post :create, params: { link: attributes_for(:link) }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(link_url(Link.last)) }
      it { expect(flash[:notice]).to eq('Link was successfully created.') }
    end

    context 'with invalid data' do
      before do
        login_user(user)
        post :create, params: { link: attributes_for(:link, :invalid) }
      end

      it { expect(response).to have_http_status(422) }
    end
  end

  describe 'PATCH #update' do
    context 'with valid data' do
      before do
        login_user(user)
        patch :update, params: { id: link.id, link: attributes_for(:link, :updated) }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(link_url(link)) }
      it { expect(flash[:notice]).to eq('Link was successfully updated.') }
    end

    context 'with invalid data' do
      before do
        login_user(user)
        patch :update, params: { id: link.id, link: attributes_for(:link, :invalid) }
      end

      it { expect(response).to have_http_status(422) }
    end
  end

  describe 'DELETE #destroy' do
    before do
      login_user(user)
      delete :destroy, params: { id: link.id }
    end

    it { expect(response).to have_http_status(302) }
    it { expect(response).to redirect_to(links_url) }
    it { expect(flash[:notice]).to eq('Link was successfully destroyed.') }
  end
end
