require 'rails_helper'

RSpec.describe InstallationsController, type: :controller do
  describe 'GET #new' do
    before do
      get :new
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'POST #create' do
    context 'with valid data' do
      before do
        post :create, params: { user: attributes_for(:user) }
      end

      it { expect(response).to have_http_status(302) }
      it { expect(response).to redirect_to(links_url) }
      it { expect(flash[:notice]).to eq('Instalação feita com sucesso!!!') }
    end

    context 'with invalid data' do
      before do
        post :create, params: { user: attributes_for(:user, :invalid) }
      end

      it { expect(response).to have_http_status(422) }
    end
  end
end
