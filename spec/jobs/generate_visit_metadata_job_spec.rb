require 'rails_helper'

describe GenerateVisitMetadataJob, type: :job do
  describe '#perform_now' do
    let(:visit) { create(:visit) }

    it 'sets the visit`s metadata' do
      ActiveJob::Base.queue_adapter = :test
      GenerateVisitMetadataJob.perform_now(visit.id)

      visit.reload

      expect(visit.browser).to eq('Internet Explorer')
      expect(visit.location).to eq('Poprad/Slovak Republic')
      expect(visit.platform).to eq('Windows')
      expect(visit.source).to eq('Acesso Direto')
    end
  end
end
