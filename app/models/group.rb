class Group < ApplicationRecord
  has_many :links, dependent: :destroy

  validates :name, presence: true

  def self.sorted
    order('lower(name) ASC')
  end
end
