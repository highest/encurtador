class User < ApplicationRecord
  authenticates_with_sorcery!

  attr_accessor :current_password

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :password, length: { minimum: 6, allow_blank: true }, confirmation: true
  validates :password, presence: true, on: :create
  validates :password, presence: true, on: :change_password

  def update_with_password(attributes, options = {})
    assign_attributes(attributes)

    if valid_password?(attributes[:current_password])
      save(options)
    else
      valid?(options)
      errors.add(:current_password, :invalid, message: 'incorreta')
      false
    end
  end
end
