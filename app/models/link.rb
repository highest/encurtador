class Link < ApplicationRecord
  has_many :visits, dependent: :destroy
  belongs_to :group, counter_cache: true

  validates :name, presence: true
  validates :original_url, presence: true
  validates :slug, presence: true, uniqueness: true
  accepts_nested_attributes_for :group, reject_if: :should_create_group?

  after_initialize :generate_slug

  def generate_slug
    return if slug.present?

    self.slug = random_string
  end

  def random_string(size = 8)
    chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    Array.new(size) { chars[rand(chars.size)] }.join
  end

  def should_create_group?
    group_id?
  end

  def self.most_visited(limit = nil)
    collection = order('visits_count DESC')
    collection = collection.limit(limit) if limit
    collection
  end
end
