class Visit < ApplicationRecord
  belongs_to :link, counter_cache: true

  def generate_browser_and_platform
    user_agent = UserAgent.parse(self.user_agent)
    self.browser = user_agent.browser
    self.platform = user_agent.platform
  end

  def generate_location
    location = Geocoder.search(ip)[0]
    self.location = 'Não Identificado'
    self.location = "#{location.city}/#{location.country}" if location && location.city.present?
  end

  def generate_source
    self.source = get_host_without_www(referer)
  end

  def get_host_without_www(url)
    return 'Acesso Direto' if url.blank?

    uri = URI.parse(url)
    uri = URI.parse("http://#{url}") if uri.scheme.nil?
    host = uri.host.downcase
    host.start_with?('www.') ? host[4..-1] : host
  end

  def generate_metadata
    generate_browser_and_platform
    generate_location
    generate_source
  end

  def self.recent(limit = nil)
    collection = order('created_at DESC')

    return collection unless limit

    collection.limit(limit)
  end
end
