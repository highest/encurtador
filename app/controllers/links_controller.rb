class LinksController < ApplicationController
  skip_before_action :require_login, only: [:visit]
  before_action :set_link, only: [:show, :edit, :update, :destroy]

  def index
    @links = Link.most_visited
  end

  def show
    @start_period = Time.zone.today.last_month
    @end_period = Time.zone.today

    @start_period = params[:start_period].to_date if params[:start_period]
    @end_period = params[:end_period].to_date if params[:end_period]

    @visits = @link.visits.where('created_at BETWEEN ? AND ?', @start_period, @end_period)
  end

  def visit
    @link = Link.find_by!(slug: params[:slug])
    @visit = @link.visits.build(
      ip: request.remote_ip,
      referer: request.referer,
      user_agent: request.user_agent
    )
    @visit.save!

    GenerateVisitMetadataJob.perform_later(@visit.id)

    redirect_to @link.original_url, status: 301
  end

  def new
    @link = Link.new(new_link_params)
  end

  def edit
  end

  def create
    @link = Link.new(new_link_params)

    if @link.save
      redirect_to @link, notice: 'Link was successfully created.'
    else
      render :new, status: 422
    end
  end

  def update
    if @link.update(edit_link_params)
      redirect_to @link, notice: 'Link was successfully updated.'
    else
      render :edit, status: 422
    end
  end

  def destroy
    @link.destroy
    redirect_to links_url, notice: 'Link was successfully destroyed.'
  end

  private

  def set_link
    @link = Link.find(params[:id])
  end

  def new_link_params
    params
      .require(:link).permit(:name, :original_url, :slug, :group_id, group_attributes: [:name])
  end

  def edit_link_params
    params.require(:link).permit(:name, :group_id, group_attributes: [:name])
  end
end
