class DashboardController < ApplicationController
  def show
    @start_period = Time.zone.today.last_month
    @end_period = Time.zone.today.end_of_day

    @start_period = params[:start_period].to_date if params[:start_period]
    @end_period = params[:end_period].to_date if params[:end_period]

    @visits = Visit.where('created_at BETWEEN ? AND ?', @start_period, @end_period)
    @link_visits_in_period = @visits.group(:link_id).count
    @links = Link.where(id: @link_visits_in_period.keys).most_visited(20)

    @links.each do |link|
      link.visits_count = @link_visits_in_period[link.id]
    end
  end
end
