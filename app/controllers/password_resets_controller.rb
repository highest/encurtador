class PasswordResetsController < ApplicationController
  skip_before_action :require_login
  layout 'simple'

  def new
  end

  def create
    @user = User.find_by(email: params[:email])

    if @user
      @user.deliver_reset_password_instructions!
      redirect_to(login_path, notice: 'Instruções foram enviadas para o seu e-mail')
    else
      flash.now[:alert] = 'Reset password failed'
      render :new, status: 422
    end
  end

  def edit
    @token = params[:id]
    @user = User.load_from_reset_password_token(params[:id])

    not_authenticated if @user.blank?
  end

  def update
    @token = params[:id]
    @user = User.load_from_reset_password_token(params[:id])

    not_authenticated if @user.blank?

    @user.password_confirmation = params[:user][:password_confirmation]

    if @user.change_password!(params[:user][:password])
      redirect_to(login_path, notice: 'Senha foi atualizada com sucesso.')
    else
      flash.now[:alert] = 'Edit password failed'
      render :edit, status: 422
    end
  end
end
