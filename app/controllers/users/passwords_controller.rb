class Users::PasswordsController < ApplicationController
  def edit
    @user = current_user
  end

  def update
    @user = current_user

    if @user.update_with_password(user_params, context: :change_password)
      redirect_to edit_users_password_path, notice: 'Senha atualizada com sucesso'
    else
      render :edit, status: 422
    end
  end

  def user_params
    params.require(:user).permit(:current_password, :password, :password_confirmation)
  end
end
