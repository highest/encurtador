class Users::RegistrationsController < ApplicationController
  def edit
    @user = current_user
  end

  def update
    @user = current_user

    if @user.update_with_password(user_params)
      redirect_to edit_users_registration_path, notice: 'Perfil atualizado com sucesso'
    else
      render :edit, status: 422
    end
  end

  def user_params
    params.require(:user).permit(:name, :email, :current_password)
  end
end
