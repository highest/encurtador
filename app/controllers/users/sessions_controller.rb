class Users::SessionsController < ApplicationController
  skip_before_action :require_login
  before_action :require_existing_user
  layout 'simple'

  def new
  end

  def create
    @user = login(params[:email], params[:password])

    if @user
      redirect_back_or_to(links_url, notice: 'Login successful')
    else
      flash.now[:alert] = 'Login failed'
      render :new, status: 422
    end
  end

  def destroy
    logout
    redirect_to(login_url, notice: 'Logged out!')
  end

  def require_existing_user
    redirect_to installation_path unless User.any?
  end
end
