class InstallationsController < ApplicationController
  skip_before_action :require_login
  before_action :require_no_users
  layout 'simple'

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      auto_login(@user)
      redirect_to links_path, notice: 'Instalação feita com sucesso!!!'
    else
      render :new, status: 422
    end
  end

  def require_no_users
    redirect_to login_path if User.any?
  end

  def user_params
    params.require(:user).permit(:name, :email, :password)
  end
end
