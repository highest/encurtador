class GenerateVisitMetadataJob < ApplicationJob
  def perform(visit_id)
    @visit = Visit.find(visit_id)
    @visit.generate_metadata
    @visit.save!
  end
end
