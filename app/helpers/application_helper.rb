module ApplicationHelper
  def stats_table(hash)
    table = []

    total = hash.values.sum.to_f

    hash.each do |key, value|
      percentage = total > 0 ? (value / total * 100) : 0
      table << [key, value, percentage]
    end

    table.sort_by! { |row| -row[1] }

    top = table[0..3]

    others = table[4..-1]

    if others
      top << ['Outros', others.map { |row| row[1] }.sum, others.map { |row| row[2] }.sum]
    end

    top
  end

  def visit_link_url(path, options = {})
    options[:host] = ENV['DOMAIN']
    super(path, options)
  end
end
