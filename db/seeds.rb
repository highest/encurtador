AGENT_ALIASES = {
  'Linux Firefox' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0',
  'Linux Konqueror' => 'Mozilla/5.0 (compatible; Konqueror/3; Linux)',
  'Linux Mozilla' => 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.4) Gecko/20030624',
  'Mac Firefox' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:43.0) Gecko/20100101 Firefox/43.0',
  'Mac Mozilla' => 'Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.4a) Gecko/20030401',
  'Mac Safari 4' => 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; de-at) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10',
  'Mac Safari' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9',
  'Windows Chrome' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.125 Safari/537.36',
  'Windows IE 6' => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)',
  'Windows IE 7' => 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
  'Windows IE 8' => 'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
  'Windows IE 9' => 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)',
  'Windows IE 10' => 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)',
  'Windows IE 11' => 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',
  'Windows Edge' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586',
  'Windows Mozilla' => 'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4b) Gecko/20030516 Mozilla Firebird/0.6',
  'Windows Firefox' => 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0',
  'iPhone' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B5110e Safari/601.1',
  'iPad' => 'Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
  'Android' => 'Mozilla/5.0 (Linux; Android 5.1.1; Nexus 7 Build/LMY47V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.76 Safari/537.36',
}

groups = [
  {
    name: 'Economia',
    links: [
      {name: 'Brasil perde 39,3 mil vagas de trabalho com carteira assinada em setembro', original_url: 'http://economia.uol.com.br/empregos-e-carreiras/noticias/redacao/2016/10/26/brasil-perde-393-mil-vagas-de-trabalho-com-carteira-assinada-em-setembro.htm'},
      {name: 'Luxo em aviões inclui cama de casal, banco de couro e banheiro privativo', original_url: 'http://todosabordo.blogosfera.uol.com.br/2016/10/21/luxo-em-avioes-inclui-cama-de-casal-assento-de-couro-e-banheiro-privativo/'},
      {name: 'Barril do Brent fecha em baixa de 1,59%', original_url: 'http://economia.uol.com.br/noticias/efe/2016/10/26/barril-do-brent-fecha-em-baixa-de-159.htm'},
      {name: 'Crise fiscal dos Estados e greve bancária derrubam crédito consignado', original_url: 'http://www1.folha.uol.com.br/mercado/2016/10/1826513-crise-fiscal-dos-estados-e-greve-bancaria-derrubam-credito-consignado.shtml'}
    ]
  },
  {
    name: 'Entretenimento',
    links: [
      {name: '"Back to Black": o epitáfio de Amy Winehouse faz 10 anos', original_url: 'http://blogdobarcinski.blogosfera.uol.com.br/2016/10/26/back-to-black-o-epitafio-de-amy-winehouse-faz-10-anos/'},
      {name: 'Mudança de tela: Youtubers estão invadindo os cinemas', original_url: 'http://cinema.uol.com.br/album/2016/10/09/mudanca-de-tela-youtubers-estao-invadindo-os-cinemas.htm'},
      {name: 'Lembra dos trolls? Os bonecos cabeludos marcaram a infância de muita gente', original_url: 'http://cinema.uol.com.br/album/2016/10/26/lembra-dos-trolls-os-bonecos-cabeludos-marcaram-a-infancia-de-muita-gente.htm'},
      {name: 'Anitta sexy, brilho de Céu, Tatá com Iorc: o que rolou no Prêmio Multishow', original_url: 'http://musica.uol.com.br/noticias/redacao/2016/10/25/com-surra-de-hits-e-bunda-anitta-e-o-grande-destaque-do-premio-multishow.htm'},
      {name: 'Fazer sucesso em SP após tantos nãos é inacreditável, diz Simone e Simaria', original_url: 'http://musica.uol.com.br/noticias/redacao/2016/09/11/fazer-sucesso-em-sp-apos-tantos-naos-e-inacreditavel-diz-simone-e-simaria.htm'},
      {name: 'Neymar anuncia que iniciará carreira musical: "Vai ter #Neymusico"', original_url: 'http://musica.uol.com.br/noticias/redacao/2016/09/11/neymar-anuncia-que-iniciara-carreira-musical-vai-ter-neymusico.htm'}
    ]
  },
  {
    name: 'Esporte',
    links: [
      {name: '"Trabalho para ser técnico do meu clube", diz Ceni em visita ao Sevilla', original_url: 'http://esporte.uol.com.br/futebol/ultimas-noticias/lancepress/2016/10/26/trabalho-para-ser-tecnico-do-meu-clube-diz-ceni-em-visita-ao-sevilla.htm'},
      {name: 'As semifinais da Copa do Brasil', original_url: 'http://pvc.blogosfera.uol.com.br/2016/10/26/as-semifinais-da-copa-do-brasil/'},
      {name: 'Corpo de Carlos Alberto Torres é enterrado em meio a homenagens de torcidas', original_url: 'http://esporte.uol.com.br/futebol/ultimas-noticias/2016/10/26/carlos-alberto-torres---funeral.htm'},
      {name: 'Íbis revive dias de pior do mundo e abraça tapetão pra se salvar de fiasco', original_url: 'http://esporte.uol.com.br/futebol/ultimas-noticias/2016/10/26/apos-10-anos-ibis-volta-a-ser-pior-do-mundo-mas-pode-se-salvar-no-tapetao.htm'},
      {name: 'Vice-artilheiro do BR, Pottker gera disputa entre Santos e Corinthians', original_url: 'http://esporte.uol.com.br/futebol/campeonatos/brasileiro/serie-a/ultimas-noticias/2016/10/26/vice-artilheiro-do-brasileiro-gera-disputa-entre-santos-e-corinthians.htm'},
      {name: 'Santos elege venezuelano como alvo para 2017 e tenta negociar em sigilo', original_url: 'http://esporte.uol.com.br/futebol/ultimas-noticias/lancepress/2016/10/25/santos-elege-guerra-como-prioridade-e-tenta-esconder-jogador-ate-na-vila.htm'},
      {name: 'Arena Corinthians tem novo descolamento de placa de granito', original_url: 'http://blogdoperrone.blogosfera.uol.com.br/2016/10/arena-corinthians-tem-novo-descolamento-de-placa-de-granito/'},
      {name: 'Decreto oficializa banimento do MMA na França', original_url: 'http://esporte.uol.com.br/ultimas-noticias/ag-fight/2016/10/26/decreto-oficializa-banimento-do-mma-na-franca.htm'},
      {name: 'Hocevar e Severino vão às quartas em futures', original_url: 'http://tenisbrasil.uol.com.br/noticias/45914/Hocevar-e-Severino-vao-as-quartas-em-futures/'},
      {name: 'Veja como foi vitória de Radwanska sobre Muguruza', original_url: 'http://tenisbrasil.uol.com.br/noticias/45911/Veja-como-foi-vitoria-de-Radwanska-sobre-Muguruza/'}
    ]
  },
  {
    name: 'Jogos',
    links: [
      {name: 'Carta rara de Pikachu feita em ouro sai pelo equivalente a R$ 6 mil', original_url: 'http://jogos.uol.com.br/ultimas-noticias/2016/10/25/carta-rara-de-pikachu-feita-em-ouro-sai-pelo-equivalente-a-r-6-mil.htm'},
      {name: 'Torneio internacional de "CS:GO" ocorre em São Paulo; saiba como acompanhar', original_url: 'http://jogos.uol.com.br/ultimas-noticias/2016/10/25/torneio-internacional-de-csgo-ocorre-em-sao-paulo-saiba-como-acompanhar.htm'},
      {name: 'Futuro dos videogames se inspira nos celulares para aumentar lucros', original_url: 'http://jogos.uol.com.br/ultimas-noticias/2016/09/08/futuro-dos-videogames-se-inspira-nos-celulares-para-aumentar-lucros.htm'},
      {name: 'De olho em vários públicos, Nintendo quer reconquistar o mundo com o Switch', original_url: 'http://jogos.uol.com.br/ultimas-noticias/2016/10/25/de-olho-em-varios-publicos-nintendo-quer-reconquistar-o-mundo-com-o-switch.htm'},
      {name: 'Jogo de "Super Mario" chega ao iPhone e iPad em dezembro', original_url: 'http://jogos.uol.com.br/ultimas-noticias/2016/09/07/mario-tera-jogo-para-iphone.htm'}
    ]
  },
  {
    name: 'Mundo',
    links: [
      {name: 'Os dólares de Trump vêm de Frankfurt', original_url: 'http://g1.globo.com/mundo/eleicoes-nos-eua/2016/noticia/2016/10/os-dolares-de-trump-vem-de-frankfurt.html'},
      {name: 'Dois terremotos atingem região central da Itália', original_url: 'http://g1.globo.com/mundo/noticia/2016/10/terremoto-de-magnitude-56-atinge-italia.html'},
      {name: 'Irã apresenta seu primeiro \'drone suicida\'', original_url: 'http://g1.globo.com/mundo/noticia/2016/10/ira-apresenta-seu-primeiro-drone-suicida.html'},
      {name: 'Eminem ressurge com música que ataca Donald Trump', original_url: 'http://g1.globo.com/musica/noticia/2016/10/eminem-ressurge-com-musica-que-ataca-donald-trump.html'},
      {name: 'Unicef liberta 145 crianças-soldado no Sudão do Sul', original_url: 'http://g1.globo.com/mundo/noticia/2016/10/unicef-liberta-145-criancas-soldado-no-sudao-do-sul.html'},
      {name: 'Afegã famosa por capa de revista é presa no Paquistão', original_url: 'http://g1.globo.com/mundo/noticia/2016/10/afega-famosa-por-capa-de-revista-e-presa-no-paquistao.html'},
      {name: 'Imagens do dia 26 de outubro de 201', original_url: 'http://g1.globo.com/mundo/fotos/2016/10/imagens-do-dia-26-de-outubro-de-2016.html'}
    ]
  },
  {
    name: 'Tecnologia',
    links: [
      {name: 'Amazon comemora \'dia do Kindle\' com descontos', original_url: 'http://olhardigital.uol.com.br/noticia/amazon-comemora-dia-do-kindle-com-descontos-confira/63410'},
      {name: 'Hacker invade Smart TV e flagra casal fazendo sexo', original_url: 'http://olhardigital.uol.com.br/fique_seguro/noticia/hacker-invade-smart-tv-e-flagra-casal-fazendo-sexo/58636'},
      {name: 'Anonymous ensina internautas a hackear para enfrentar o Estado Islâmico', original_url: 'http://olhardigital.uol.com.br/fique_seguro/noticia/anonymous-ensina-internautas-a-hackear-para-enfrentar-o-estado-islamico/53079'},
      {name: 'Microsoft anuncia novos teclados e mouse da linha Surface', original_url: 'http://olhardigital.uol.com.br/noticia/microsoft-anuncia-novos-acessorios-para-o-surface/63412'},
      {name: 'Oi segue TIM e elimina taxas em ligações entre operadoras', original_url: 'http://olhardigital.uol.com.br/pro/noticia/oi-segue-tim-e-elimina-taxas-em-ligacoes-entre-operadoras/52687'},
      {name: '6 recursos úteis que todo dono de um celular Motorola deve conhecer', original_url: 'http://olhardigital.uol.com.br/noticia/6-recursos-uteis-que-todo-dono-de-um-celular-motorola-deve-conhecer/55155'}
    ]
  }
]

User.create!(
  name: 'Miguel Castro',
  email: 'miguel.castro27@example.com',
  password: '10203040',
  password_confirmation: '10203040'
)

groups.each do |g|
  group = Group.create!(name: g[:name])

  g[:links].each do |l|
    Link.create!(
      name: l[:name],
      original_url: l[:original_url],
      group: group
    )
  end
end

20000.times do
  visit = Visit.create!(
    ip: FFaker::Internet.ip_v4_address,
    user_agent: AGENT_ALIASES[AGENT_ALIASES.keys.sample],
    link: Link.all.sample
  )

  visit.update_attribute('created_at', FFaker::Time.date)
end
