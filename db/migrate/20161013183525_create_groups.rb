class CreateGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :groups do |t|
      t.string :name
      t.integer :links_count, default: 0

      t.timestamps
    end
  end
end
