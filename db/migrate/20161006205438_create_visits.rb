class CreateVisits < ActiveRecord::Migration[5.0]
  def change
    create_table :visits do |t|
      t.string :user_agent
      t.string :referer
      t.string :ip
      t.string :source
      t.string :browser
      t.string :location
      t.string :platform
      t.references :link, foreign_key: true

      t.timestamps
    end
  end
end
