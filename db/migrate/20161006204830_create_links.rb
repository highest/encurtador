class CreateLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :links do |t|
      t.string :name
      t.string :original_url
      t.string :slug
      t.integer :visits_count, default: 0

      t.timestamps
    end
  end
end
