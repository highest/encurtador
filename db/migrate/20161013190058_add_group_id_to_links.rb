class AddGroupIdToLinks < ActiveRecord::Migration[5.0]
  def change
    add_reference :links, :group, foreign_key: true
  end
end
