Rails.application.routes.draw do
  get 'installation', to: 'installations#new', as: :new_installation_path
  resource :installation, only: [:new, :create]
  resources :password_resets, except: [:destroy]

  get 'login' => 'users/sessions#new', as: :login
  delete 'logout' => 'users/sessions#destroy', as: :logout

  namespace :users do
    resources :sessions
    resource :registration
    resource :password
  end

  resources :links
  resources :groups

  get ':slug', to: 'links#visit', as: :visit_link
  root to: 'dashboard#show'
end
