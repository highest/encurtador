require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

Dotenv::Railtie.load

module Encurtador
  class Application < Rails::Application
    config.active_job.queue_adapter = :sidekiq if Rails.env.production?
  end
end
