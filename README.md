# CNL Encurtador

Um mundo de links inteligentes.

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/codenomel/encurtador)


List of Possible Configurations:
- Shorten domain (goo.gl)
- Default slug length, default: 3
- Characters used to shorten urls, default: 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-
- Enable statistics, default: true
- Display location statistics, default: true
- Display browser statistics, default: true
- Display platform statistics, default: true
- Display source statistics, default: true
- Display access history statistics, default: true
- Display recent access, default: true
- Enable access CSV download, default: true

Done:
- Crud users (com invitation link)
- Empty screens design
- Visits view
- Background job para location
- Meus dados/Editar perfil/Dados de acesso
- Redirect 301
- Não permitir que slugs seram alterados
- Exibir nome do grupo na tela de visualização do link
- Finalizar layout com Bootstrap
- Deletar link
- Rails i18n (pt-br, en)
- Colocar sidekiq apenas para ambiente de produção
- Resetar senha
- Criação do grupo através da tela de criação de link
- Adicionar datepicker para aplicar o range de datas no dashboard

Doing:
- Organizar pastas e arquivos

Todo:
- Commits e Readme em inglês
- Escrever testes


Ideias:
- get_host_without_www add creditos do stackoverflow
- Limitar número de pontos que cada projeto pode ter
- Podemos retornar projetos
- Audio em dialogo
